<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\Author;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthorController extends Controller
{
    public function index(Request $request)
    {
        $authors = Author::orderBy('created_at', 'DESC')->paginate(10);

        return response()->json([
            'status' => 200,
            'message' => 'Author retrieved successfully',
            'data' => [
                'authors' => $authors
            ]
        ], 200);
    }

    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'status' => 422,
                'error' => $validate->errors()->first()
            ], 422);
        }

        $author = Author::create(['name' => $request->name]);

        return response()->json([
            'status' => 201,
            'message' => 'Author created successfully',
            'data' => $author
        ], 201);
    }

    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'status' => 422,
                'error' => $validate->errors()->first()
            ], 422);
        }

        $author = Author::where('id', $id)->first();
        if (!$author) {
            return response()->json([
                'status' => 404,
                'error' => "Author not found"
            ], 404);
        }
        $author->name = $request->name;
        $author->save();

        return response()->json([
            'status' => 200,
            'message' => 'Author updated successfully',
            'data' => $author
        ], 200);
    }

    public function destroy(Request $request, $id)
    {
        $author = Author::where('id', $id)->first();
        if (!$author) {
            return response()->json([
                'status' => 404,
                'error' => "Author not found"
            ], 404);
        }
        $author->delete();

        return response()->json([
            'status' => 200,
            'message' => 'Author deleted successfully',
            'data' => []
        ], 200);
    }
}
