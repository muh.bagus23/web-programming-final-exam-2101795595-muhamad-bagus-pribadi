<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function index(Request $request)
    {
        $books = Book::with('author', 'publisher')->when(request()->search, function($q){
            $q->where('books.title', 'like', '%'.request()->search.'%');
            $q->orWhereHas('author', function($q){
                $q->where('authors.name', 'like', '%'.request()->search.'%');
            });
            $q->orWhereHas('publisher', function($q){
                $q->where('publishers.name', 'like', '%'.request()->search.'%');
            });
        })->orderBy('created_at', 'DESC')->paginate(16);

        return response()->json([
            'status' => 200,
            'message' => 'Book retrieved successfully',
            'data' => [
                'books' => $books
            ]
        ], 200);
    }

    public function show($id)
    {
        $book = Book::with('author', 'publisher')->where('id', $id)->first();

        return response()->json([
            'status' => 200,
            'message' => 'Book retrieved successfully',
            'data' => $book
        ], 200);
    }

    public function destroy(Request $request, $id)
    {
        $book = Book::where('id', $id)->first();
        if (!$book) {
            return response()->json([
                'status' => 404,
                'error' => "Author not found"
            ], 404);
        }
        $book->delete();

        return response()->json([
            'status' => 200,
            'message' => 'Book deleted successfully',
            'data' => []
        ], 200);
    }
}
