<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\Publisher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PublisherController extends Controller
{
    public function index(Request $request)
    {
        $publishers = Publisher::orderBy('created_at', 'DESC')->paginate(10);

        return response()->json([
            'status' => 200,
            'message' => 'Publisher retrieved successfully',
            'data' => [
                'publishers' => $publishers
            ]
        ], 200);
    }

    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'status' => 422,
                'error' => $validate->errors()->first()
            ], 422);
        }

        $publisher = Publisher::create(['name' => $request->name]);

        return response()->json([
            'status' => 201,
            'message' => 'Publisher created successfully',
            'data' => $publisher
        ], 201);
    }

    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'status' => 422,
                'error' => $validate->errors()->first()
            ], 422);
        }

        $publisher = Publisher::where('id', $id)->first();
        if (!$publisher) {
            return response()->json([
                'status' => 404,
                'error' => "Author not found"
            ], 404);
        }
        $publisher->name = $request->name;
        $publisher->save();

        return response()->json([
            'status' => 200,
            'message' => 'Publisher updated successfully',
            'data' => $publisher
        ], 200);
    }

    public function destroy(Request $request, $id)
    {
        $publisher = Publisher::where('id', $id)->first();
        if (!$publisher) {
            return response()->json([
                'status' => 404,
                'error' => "Author not found"
            ], 404);
        }
        $publisher->delete();

        return response()->json([
            'status' => 200,
            'message' => 'Publisher deleted successfully',
            'data' => []
        ], 200);
    }
}
