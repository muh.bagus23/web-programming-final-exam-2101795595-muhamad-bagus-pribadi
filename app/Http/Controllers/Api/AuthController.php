<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'email' => 'required|exists:users,email',
            'password' => 'required'
        ]);

        if ($validate->fails()) {
            return response()->json([
                'status' => 422,
                'error' => $validate->errors()->first()
            ], 422);
        }

        $getUser = User::where('email', $request->email)->first();
        if (!$getUser) {
            return response()->json([
                'status' => 422,
                'error' => "Email not registered"
            ], 422);
        }

        if (!Hash::check($request->password, $getUser->password)) {
            return response()->json([
                'status' => 422,
                'error' => "Wrong password"
            ], 422);
        }

        return response()->json([
            'status' => 201,
            'message' => 'success',
            'data' => [
                'token' => JWTAuth::fromUser($getUser),
                'user' => [
                    'id' => $getUser->id,
                    'email' => $getUser->email,
                    'name' => $getUser->name
                ]
            ]
        ], 201);
    }
}
