<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Author;
use App\Models\Book;
use App\Models\Publisher;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::create([
            'name' => 'Administrator',
            'email' => 'admin@bookrent.com',
            'password' => Hash::make('12345678'),
            'role' => 'admin'
        ]);

        User::create([
            'name' => 'John Doe',
            'email' => 'johndoe@example.com',
            'password' => Hash::make('12345678'),
            'role' => 'user'
        ]);

        for ($i=0; $i < 10; $i++) {
            Author::create([
                'name' => fake()->name()
            ]);

            Publisher::create([
                'name' => fake()->company()
            ]);
        }

        for ($i=0; $i < 50; $i++) { 
            Book::create([
                'title' => fake()->words(3, true),
                'description' => fake()->sentences(5, true),
                'status' => collect(['rented', 'broken', 'available'])->random(),
                'stock' => rand(0, 5),
                'author_id' => Author::orderByRaw('RAND()')->first()->id,
                'publisher_id' => Publisher::orderByRaw('RAND()')->first()->id,
                'image_url' => fake()->imageUrl(400, 600, 'book')
            ]);
        }
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
