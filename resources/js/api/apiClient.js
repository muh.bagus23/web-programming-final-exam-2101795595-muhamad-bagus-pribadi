import axios from 'axios';

const axiosClient = axios.create({
    baseURL: 'http://localhost:8001/api',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
});

axiosClient.interceptors.response.use(
    function (response) {
        return response.data;
    }, 
    function (error) {
        let res = error.response;
        if (res.status == 401) {
            window.location.href = 'http://localhost:8001';
        }
        return Promise.reject(res);
    }
);

export default axiosClient