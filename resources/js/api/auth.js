import axiosClient from "./apiClient";

export function login(data){
    return axiosClient.post('/auth/login', JSON.stringify(data))
}