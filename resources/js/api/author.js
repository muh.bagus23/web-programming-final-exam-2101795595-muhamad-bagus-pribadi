import axiosClient from "./apiClient";

export function listAuthor(page){
    return axiosClient.get('/admin/author?page='+page)
}

export function createAuthor(data){
    return axiosClient.post('/admin/author', JSON.stringify(data))
}

export function updateAuthor(data){
    return axiosClient.put('/admin/author/'+data.id, JSON.stringify(data))
}

export function deleteAuthor(data){
    return axiosClient.delete('/admin/author/'+data.id, JSON.stringify(data))
}