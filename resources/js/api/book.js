import axiosClient from "./apiClient";

export function listBook(data){
    return axiosClient.get('/admin/book?page='+data.page+'&search='+data.search)
}

export function detailBook(data){
    return axiosClient.get('/admin/book/'+data.id)
}

export function createBook(data){
    return axiosClient.post('/admin/book', JSON.stringify(data))
}

export function updateBook(data){
    return axiosClient.put('/admin/book/'+data.id, JSON.stringify(data))
}

export function deleteBook(data){
    return axiosClient.delete('/admin/book/'+data.id, JSON.stringify(data))
}