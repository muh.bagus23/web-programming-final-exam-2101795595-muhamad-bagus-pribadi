import axiosClient from "./apiClient";

export function listPublisher(page){
    return axiosClient.get('/admin/publisher?page='+page)
}

export function createPublisher(data){
    return axiosClient.post('/admin/publisher', JSON.stringify(data))
}

export function updatePublisher(data){
    return axiosClient.put('/admin/publisher/'+data.id, JSON.stringify(data))
}

export function deletePublisher(data){
    return axiosClient.delete('/admin/publisher/'+data.id, JSON.stringify(data))
}