import { createWebHistory, createRouter } from 'vue-router'
import store from '@/store'

/* Guest Component */
const Login = () => import('@/components/Login.vue')
const Register = () => import('@/components/Register.vue')
/* Guest Component */

/* Layouts */
const AdminLayout = () => import('@/components/layouts/Admin.vue')
/* Layouts */

/* Authenticated Component */
const Dashboard = () => import('@/components/admins/Dashboard.vue')
const Author = () => import('@/components/admins/Author.vue')
const Publisher = () => import('@/components/admins/Publisher.vue')
const Book = () => import('@/components/admins/Book.vue')
const BookDetail = () => import('@/components/admins/BookDetail.vue')
/* Authenticated Component */


const routes = [
    {
        name: "login",
        path: "/login",
        component: Login,
        meta: {
            middleware: "guest",
            title: "Login"
        }
    },
    {
        name: "register",
        path: "/register",
        component: Register,
        meta: {
            middleware: "guest",
            title: "Register"
        }
    },
    {
        path: "/dashboard",
        component: AdminLayout,
        meta: {
            middleware: "auth"
        },
        children: [
            {
                name: "dashboard",
                path: '/dashboard',
                component: Dashboard,
                meta: {
                    title: "Dashboard"
                }
            },
            {
                name: "dashboard.books",
                path: '/dashboard/books',
                component: Book,
                meta: {
                    title: "Books"
                }
            },
            {
                name: "dashboard.books.detail",
                path: '/dashboard/books/:id',
                component: BookDetail,
                meta: {
                    title: "Book Detail"
                }
            },
            {
                name: "dashboard.authors",
                path: '/dashboard/authors',
                component: Author,
                meta: {
                    title: "Authors"
                }
            },
            {
                name: "dashboard.publishers",
                path: '/dashboard/publishers',
                component: Publisher,
                meta: {
                    title: "Publishers"
                }
            },
        ]
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes, // short for `routes: routes`
})

router.beforeEach((to, from, next) => {
    document.title = to.meta.title
    if (to.meta.middleware == "guest") {
        if (localStorage.getItem('userToken')) {
            next({ name: "dashboard" })
        }else{
            next()
        }
    } else {
        if (localStorage.getItem('userToken')) {
            next()
        } else {
            next({ name: "login" })
        }
    }
})

export default router