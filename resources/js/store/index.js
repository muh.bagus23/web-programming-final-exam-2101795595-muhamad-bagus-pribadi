import { createStore, createLogger } from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import auth from './modules/auth'
import author from './modules/author'
import publisher from './modules/publisher'
import book from './modules/book'

const debug = process.env.NODE_ENV !== 'production'

export default createStore({
  modules: {
    auth,
    author,
    publisher,
    book
  },
  strict: debug,
  plugins: debug ? [createLogger()] : []
})