import router from '@/router';

const state = {
    authenticated: false,
    user: {}
}

const getters = {
    authenticated: (state) => {
        return state.authenticated
    },
    user: (state) => {
        return state.user
    }
}

const mutations = {
    setAuthenticated: (state, value) => {
        state.authenticated = value
    },
    setUser: (state, value) => {
        state.user = value
    }
}

const actions = {
    login: ({commit, state}, data) => {
        commit('setAuthenticated', true)
        commit('setUser', data.user)

        localStorage.setItem('userToken', data.token)
        localStorage.setItem('userData', JSON.stringify(data.user))

        router.push({ name: 'dashboard' })
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}