import { 
    listAuthor as listAuthorAPI, 
    createAuthor as createAuthorAPI, 
    updateAuthor as updateAuthorAPI, 
    deleteAuthor as deleteAuthorAPI 
    } from "@/api/author";

const state = {
    authors: [],
    validationMessage: null
}

const getters = {
    authors: (state) => {
        return state.authors
    },
    validationMessage: (state) => {
        return state.validationMessage
    }
}

const mutations = {
    setListAuthors: (state, value) => {
        state.authors = value
    },
    setValidationMessage: (state, value) => {
        state.validationMessage = value
    }
}

const actions = {
    list({commit, state}, payload){
        listAuthorAPI(payload)
            .then((response) => {
                commit('setListAuthors', response.data.authors)
            })
    },
    async create({ commit, state }, payload){
        createAuthorAPI(payload)
            .then((response) => {
                commit('setValidationMessage', {
                    alert: 'alert-success',
                    message: response.message
                })
            })
            .catch((response) => {
                commit('setValidationMessage', {
                    alert: 'alert-danger',
                    message: response.message
                })
            })
    },
    async delete({ commit, state }, payload){
        deleteAuthorAPI({id: payload})
            .then((response) => {
                commit('setValidationMessage', {
                    alert: 'alert-success',
                    message: response.message
                })
            })
            .catch((response) => {
                commit('setValidationMessage', {
                    alert: 'alert-danger',
                    message: response.message
                })
            })
    },
    async update({ commit, state }, payload){
        updateAuthorAPI(payload)
            .then((response) => {
                commit('setValidationMessage', {
                    alert: 'alert-success',
                    message: response.message
                })
            })
            .catch((response) => {
                commit('setValidationMessage', {
                    alert: 'alert-danger',
                    message: response.message
                })
            })
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}