import { 
    listBook as listBookAPI,
    deleteBook as deleteBookAPI,
    detailBook as detailBookAPI
    } from "@/api/book";

const state = {
    books: [],
    book: {},
    validationMessage: null
}

const getters = {
    books: (state) => {
        return state.books
    },
    book: (state) => {
        return state.book
    },
    validationMessage: (state) => {
        return state.validationMessage
    }
}

const mutations = {
    setListBooks: (state, value) => {
        state.books = value
    },
    setDetailBook: (state, value) => {
        state.book = value
    },
    setValidationMessage: (state, value) => {
        state.validationMessage = value
    }
}

const actions = {
    async list({commit, state}, payload){
        listBookAPI(payload)
            .then((response) => {
                commit('setListBooks', response.data.books)
            })
    },
    async delete({ commit, state }, payload){
        deleteBookAPI({id: payload})
            .then((response) => {
                commit('setValidationMessage', {
                    alert: 'alert-success',
                    message: response.message
                })
            })
            .catch((response) => {
                commit('setValidationMessage', {
                    alert: 'alert-danger',
                    message: response.message
                })
            })
    },
    async detail({ commit, state }, payload){
        detailBookAPI(payload)
            .then((response) => {
                commit('setDetailBook', response.data)
            })
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}