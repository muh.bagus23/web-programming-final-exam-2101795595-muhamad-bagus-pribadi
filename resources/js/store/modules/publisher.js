import { 
    listPublisher as listPublisherAPI, 
    createPublisher as createPublisherAPI, 
    updatePublisher as updatePublisherAPI, 
    deletePublisher as deletePublisherAPI 
    } from "@/api/publisher";

const state = {
    publishers: [],
    validationMessage: null
}

const getters = {
    publishers: (state) => {
        return state.publishers
    },
    validationMessage: (state) => {
        return state.validationMessage
    }
}

const mutations = {
    setListPublishers: (state, value) => {
        state.publishers = value
    },
    setValidationMessage: (state, value) => {
        state.validationMessage = value
    }
}

const actions = {
    list({commit, state}, payload){
        listPublisherAPI(payload)
            .then((response) => {
                commit('setListPublishers', response.data.publishers)
            })
    },
    async create({ commit, state }, payload){
        createPublisherAPI(payload)
            .then((response) => {
                commit('setValidationMessage', {
                    alert: 'alert-success',
                    message: response.message
                })
            })
            .catch((response) => {
                commit('setValidationMessage', {
                    alert: 'alert-danger',
                    message: response.message
                })
            })
    },
    async delete({ commit, state }, payload){
        deletePublisherAPI({id: payload})
            .then((response) => {
                commit('setValidationMessage', {
                    alert: 'alert-success',
                    message: response.message
                })
            })
            .catch((response) => {
                commit('setValidationMessage', {
                    alert: 'alert-danger',
                    message: response.message
                })
            })
    },
    async update({ commit, state }, payload){
        updatePublisherAPI(payload)
            .then((response) => {
                commit('setValidationMessage', {
                    alert: 'alert-success',
                    message: response.message
                })
            })
            .catch((response) => {
                commit('setValidationMessage', {
                    alert: 'alert-danger',
                    message: response.message
                })
            })
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}